package sample

import kotlin.test.Test

class FooBar(val s: String, val i: Int, val b: Boolean)

class SampleTest {
    @Test
    fun writeObject() {
        val fooBar = FooBar("We Didn't Start the Fire", 1989, true)
        console.dir(fooBar)
        TestFirebaseEnv().firestore.collection("test").doc("id").set(fooBar)
    }
}