package sample

import com.firebase.wrappers.admin.Admin
import com.firebase.wrappers.admin.FirebaseAppOptions
import com.firebase.wrappers.admin.firestore.Firestore
import require

class TestFirebaseEnv {
    val functions = require("firebase-functions-test")
    val admin = require("firebase-admin")
    val firestore: Firestore
    val env: dynamic

    init {
        val appOptions = FirebaseAppOptions(
            databaseURL = "databaseURL",
            storageBucket = "storageBucket",
            projectId = "projectId")

        env = functions(appOptions, "JSON_FILE")

        admin.initializeApp()
        firestore = Admin.firestore()
    }

    fun cleanup() {
        env.cleanup()
    }
}