package com.firebase.wrappers.admin.firestore

import kotlin.js.Promise

external interface CollectionReference : Query {
    val id: String
    val parent: DocumentReference?
    val path: String
    fun collection(path: String): CollectionReference
    fun doc(documentPath: String?): DocumentReference
    fun add(data: Any): Promise<DocumentReference>
}