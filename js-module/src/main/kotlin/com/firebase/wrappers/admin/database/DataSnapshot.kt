package com.firebase.wrappers.admin.database


external interface DataSnapshot {
    val key: String?
    val ref: Reference
    fun child(path: String): DataSnapshot
    fun exists(): Boolean
    fun exportType(): Any?
    fun forEach(action: () -> Unit): Boolean
    fun getPriority(): Any?
    fun hasChild(path: String): Boolean
    fun hasChildren(): Boolean
    fun numChildren(): Int
    fun toJSON(): Any?

    @JsName("val")
    fun <T> value(): T
}