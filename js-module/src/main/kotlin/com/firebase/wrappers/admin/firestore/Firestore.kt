@file:JsQualifier("admin.firestore")
package com.firebase.wrappers.admin.firestore

import kotlin.js.Promise

external interface Firestore {
    fun collection(collectionPath: String): CollectionReference
    fun batch(): WriteBatch
}

external interface WriteBatch {

    /**
     * Create the document referred to by the provided `DocumentReference`. The
     * operation will fail the batch if a document exists at the specified
     * location.
     *
     * @param documentRef A reference to the document to be created.
     * @param data The object data to serialize as the document.
     * @return This `WriteBatch` instance. Used for chaining method calls.
     */
    fun create(documentRef: DocumentReference, data: Any): WriteBatch

    /**
     * Write to the document referred to by the provided `DocumentReference`.
     * If the document does not exist yet, it will be created. If you pass
     * `SetOptions`, the provided data can be merged into the existing document.
     *
     * @param documentRef A reference to the document to be set.
     * @param data An object of the fields and values for the document.
     * @param options An object to configure the set behavior.
     * @return This `WriteBatch` instance. Used for chaining method calls.
     */
    fun set(documentRef: DocumentReference, data: Any, options: SetOptions?): WriteBatch

    /**
     * Update fields of the document referred to by the provided
     * `DocumentReference`. If the document doesn't yet exist, the update fails
     * and the entire batch will be rejected.
     *
     * Nested fields can be updated by providing dot-separated field path
     * strings.
     *
     * @param documentRef A reference to the document to be updated.
     * @param data An object containing the fields and values with which to
     * update the document.
     * @param precondition A Precondition to enforce on this update.
     * @return This `WriteBatch` instance. Used for chaining method calls.
     */
    fun update(documentRef: DocumentReference, data: Any, precondition: Any?): WriteBatch

    /**
     * Updates fields in the document referred to by the provided
     * `DocumentReference`. The update will fail if applied to a document that
     * does not exist.
     *
     * Nested fields can be updated by providing dot-separated field path
     * strings or by providing FieldPath objects.
     *
     * A `Precondition` restricting this update can be specified as the last
     * argument.
     *
     * @param documentRef A reference to the document to be updated.
     * @param field The first field to update.
     * @param value The first value
     * @param fieldsOrPrecondition An alternating list of field paths and values
     * to update, optionally followed a `Precondition` to enforce on this update.
     * @return This `WriteBatch` instance. Used for chaining method calls.
     */
    fun update(documentRef: DocumentReference, field: String, value: Any, vararg fieldsOrPrecondition: Any): WriteBatch;

    /**
     * Deletes the document referred to by the provided `DocumentReference`.
     *
     * @param documentRef A reference to the document to be deleted.
     * @param precondition A Precondition to enforce for this delete.
     * @return This `WriteBatch` instance. Used for chaining method calls.
     */
    fun delete(documentRef: DocumentReference, precondition: Any?): WriteBatch

    fun delete(documentRef: DocumentReference): WriteBatch

    /**
     * Commits all of the writes in this write batch as a single atomic unit.
     *
     * @return A Promise resolved once all of the writes in the batch have been
     * successfully written to the backend as an atomic unit.
     */
    fun commit(): Promise<Array<WriteResult>>
}


external interface WriteResult {
    /**
     * The write time as set by the Firestore servers. Formatted as an ISO-8601
     * string.
     */
    val writeTime: String
}