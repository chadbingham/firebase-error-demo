package com.firebase.wrappers.admin.firestore

import kotlin.js.Promise

external interface DocumentReference {
    val id: String
    val parent: DocumentReference
    val path: String

    fun collection(collectionPath: String): CollectionReference
    fun delete(): Promise<Unit>
    fun get(): Promise<DocumentSnapshot>
    fun set(value: Any): Promise<Unit>
    fun set(map: Map<String, Any>, setOptions: SetOptions): Promise<Unit>
}

external interface Query {

    /**
     * The `Firestore` for the Firestore database (useful for performing
     * transactions, etc.).
     */
    val firestore: Firestore;

    /**
     * Creates and returns a new Query with the additional filter that documents
     * must contain the specified field and that its value should satisfy the
     * relation constraint provided.
     *
     * This function returns a new (immutable) instance of the Query (rather
     * than modify the existing instance) to impose the filter.
     *
     * @param fieldPath The path to compare
     * @param opStr The operation string (e.g "<", "<=", "==", ">", ">=").
     * @param value The value for comparison
     * @return The created Query.
     */
    fun where(fieldPath: String, opStr: String, value: Any): Query

    /**
     * Creates and returns a new Query that's additionally sorted by the
     * specified field, optionally in descending order instead of ascending.
     *
     * This function returns a new (immutable) instance of the Query (rather
     * than modify the existing instance) to impose the order.
     *
     * @param fieldPath The field to sort by.
     * @param directionStr Optional direction to sort by ('asc' or 'desc'). If
     * not specified, order will be ascending.
     * @return The created Query.
     */
    fun orderBy(fieldPath: String, directionStr: String?): Query

    /**
     * Creates and returns a new Query that's additionally limited to only
     * return up to the specified number of documents.
     *
     * This function returns a new (immutable) instance of the Query (rather
     * than modify the existing instance) to impose the limit.
     *
     * @param limit The maximum number of items to return.
     * @return The created Query.
     */
    fun limit(limit: Number): Query

    /**
     * Specifies the offset of the returned results.
     *
     * This function returns a new (immutable) instance of the Query (rather
     * than modify the existing instance) to impose the offset.
     *
     * @param offset The offset to apply to the Query results.
     * @return The created Query.
     */
    fun offset(offset: Number): Query

    /**
     * Creates and returns a new Query instance that applies a field mask to
     * the result and returns only the specified subset of fields. You can
     * specify a list of field paths to return, or use an empty list to only
     * return the references of matching documents.
     *
     * This function returns a new (immutable) instance of the Query (rather
     * than modify the existing instance) to impose the field mask.
     *
     * @param field The field paths to return.
     * @return The created Query.
     */
    fun select(vararg field: Any): Query

    /**
     * Creates and returns a new Query that starts at the provided document
     * (inclusive). The starting position is relative to the order of the query.
     * The document must contain all of the fields provided in the orderBy of
     * this query.
     *
     * @param snapshot The snapshot of the document to start after.
     * @return The created Query.
     */
    fun startAt(snapshot: DocumentSnapshot): Query

    /**
     * Creates and returns a new Query that starts at the provided fields
     * relative to the order of the query. The order of the field values
     * must match the order of the order by clauses of the query.
     *
     * @param fieldValues The field values to start this query at, in order
     * of the query's order by.
     * @return The created Query.
     */
    fun startAt(vararg fieldValues: Array<Any>): Query;

    /**
     * Creates and returns a new Query that starts after the provided document
     * (exclusive). The starting position is relative to the order of the query.
     * The document must contain all of the fields provided in the orderBy of
     * this query.
     *
     * @param snapshot The snapshot of the document to start after.
     * @return The created Query.
     */
    fun startAfter(snapshot: DocumentSnapshot): Query

    /**
     * Creates and returns a new Query that starts after the provided fields
     * relative to the order of the query. The order of the field values
     * must match the order of the order by clauses of the query.
     *
     * @param fieldValues The field values to start this query after, in order
     * of the query's order by.
     * @return The created Query.
     */
    fun startAfter(vararg fieldValues: Any): Query

    /**
     * Creates and returns a new Query that ends before the provided document
     * (exclusive). The end position is relative to the order of the query. The
     * document must contain all of the fields provided in the orderBy of this
     * query.
     *
     * @param snapshot The snapshot of the document to end before.
     * @return The created Query.
     */
    fun endBefore(snapshot: DocumentSnapshot): Query

    /**
     * Creates and returns a new Query that ends before the provided fields
     * relative to the order of the query. The order of the field values
     * must match the order of the order by clauses of the query.
     *
     * @param fieldValues The field values to end this query before, in order
     * of the query's order by.
     * @return The created Query.
     */
    fun endBefore(vararg fieldValues: Any): Query

    /**
     * Creates and returns a new Query that ends at the provided document
     * (inclusive). The end position is relative to the order of the query. The
     * document must contain all of the fields provided in the orderBy of this
     * query.
     *
     * @param snapshot The snapshot of the document to end at.
     * @return The created Query.
     */
    fun endAt(snapshot: DocumentSnapshot): Query

    /**
     * Creates and returns a new Query that ends at the provided fields
     * relative to the order of the query. The order of the field values
     * must match the order of the order by clauses of the query.
     *
     * @param fieldValues The field values to end this query at, in order
     * of the query's order by.
     * @return The created Query.
     */
    fun endAt(vararg fieldValues: Any): Query

    /**
     * Executes the query and returns the results as a `QuerySnapshot`.
     *
     * @return A Promise that will be resolved with the results of the Query.
     */
    fun get(): Promise<QuerySnapshot>

}

interface QueryDocumentSnapshot : DocumentSnapshot


/**
 * A `DocumentChange` represents a change to the documents matching a query.
 * It contains the document affected and the type of change that occurred.
 */
interface DocumentChange {
    /** The type of change ('added', 'modified', or 'removed'). */
    val type: String

    /** The document affected by this change. */
    val doc: QueryDocumentSnapshot

    /**
     * The index of the changed document in the result set immediately prior to
     * this DocumentChange (i.e. supposing that all prior DocumentChange objects
     * have been applied). Is -1 for 'added' events.
     */
    val oldIndex: Number

    /**
     * The index of the changed document in the result set immediately after
     * this DocumentChange (i.e. supposing that all prior DocumentChange
     * objects and the current DocumentChange object have been applied).
     * Is -1 for 'removed' events.
     */
    val newIndex: Number
}

/**
 * A `QuerySnapshot` contains zero or more `QueryDocumentSnapshot` objects
 * representing the results of a query. The documents can be accessed as an
 * array via the `docs` property or enumerated using the `forEach` method. The
 * number of documents can be determined via the `empty` and `size`
 * properties.
 */
interface QuerySnapshot {

    /**
     * The query on which you called `get` or `onSnapshot` in order to get this
     * `QuerySnapshot`.
     */
    val query: Query

    /**
     * An array of the documents that changed since the last snapshot. If this
     * is the first snapshot, all documents will be in the list as added
     * changes.
     */
    val docChanges: Array<DocumentChange>

    /** An array of all the documents in the QuerySnapshot. */
    val docs: Array<QueryDocumentSnapshot>

    /** The number of documents in the QuerySnapshot. */
    val size: Number

    /** True if there are no documents in the QuerySnapshot. */
    val empty: Boolean

    /** The time this query snapshot was obtained. */
    val readTime: String

    /**
     * Enumerates all of the documents in the QuerySnapshot.
     *
     * @param callback A callback to be called with a `DocumentSnapshot` for
     * each document in the snapshot.
     * @param thisArg The `this` binding for the callback.
     */
    fun forEach(callback: (result: QueryDocumentSnapshot) -> Unit, thisArg: Any?)

    fun forEach(callback: (result: QueryDocumentSnapshot) -> Unit)
}


external interface SetOptions {
    val merge: Boolean
}

val MERGE: SetOptions = object : SetOptions {
    override val merge: Boolean = true
}