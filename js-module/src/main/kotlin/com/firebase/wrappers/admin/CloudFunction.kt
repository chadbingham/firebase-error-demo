package com.firebase.wrappers.admin

// https://firebase.google.com/docs/reference/functions/functions.Event#params
// https://firebase.google.com/docs/reference/functions/functions.database.DeltaSnapshot

external interface CloudFunction<T>

external interface Event<T> {
    val params: dynamic
    val eventId: String?
    val timestamp: String?
    val eventType: String?
    val resource: String?
    val data: T
}

external interface DeltaSnapshot<T> {
    @JsName("val")
    fun value(): T
}