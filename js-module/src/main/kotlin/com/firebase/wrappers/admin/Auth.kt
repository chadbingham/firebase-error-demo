@file:JsModule("firebase-admin")
@file:JsQualifier("auth")
package com.firebase.wrappers.admin

import kotlin.js.Promise

external object Auth {
    fun verifyIdToken(idToken: String): Promise<DecodedIdToken>
    fun deleteUser(uid: String): Promise<Unit>
    fun getUser(uid: String): Promise<UserRecord>

    interface DecodedIdToken {
        var aud: String
        var auth_time: Number
        var exp: Number

        interface firebase {
            var identities: Map<String, Any>
            var sign_in_provider: String
        }

        var iat: Number
        var iss: String
        var sub: String
        var uid: String
    }

    interface UserMetadata {
        val lastSignInTime: String
        val creationTime: String
        fun toJSON(): Any
    }

    interface UserInfo {
        val uid: String
        val displayName: String
        val email: String
        val phoneNumber: String
        val photoURL: String
        val providerId: String

        fun toJSON(): Any
    }

    interface UserRecord {
        val uid: String
        val email: String
        val emailVerified: Boolean
        val displayName: String
        val phoneNumber: String
        val photoURL: String
        val disabled: Boolean
        val metadata: UserMetadata
        val providerData: UserInfo
        val passwordHash: String?
        val passwordSalt: String?
        val customClaims: Any
        val tokensValidAfterTime: String?

        fun toJSON(): Any
    }
}


