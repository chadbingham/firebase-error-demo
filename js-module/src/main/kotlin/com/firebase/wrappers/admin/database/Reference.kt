package com.firebase.wrappers.admin.database

import com.firebase.wrappers.admin.FirebaseError
import kotlin.js.Promise

external interface Reference {
    val ref: Reference
    val key: String?
    val parent: Reference
    val root: Reference

    fun child(path: String): Reference
    fun push(value: Any?, onComplete: ((FirebaseError) -> Unit)? = definedExternally) : Promise<Any>
    fun set(value: Any): Promise<Unit>
    /**
     * @param eventType One of the following strings: "value", "child_added", "child_changed", "child_removed", or "child_moved."
     */
    fun once(eventType: String): Promise<DataSnapshot>
}