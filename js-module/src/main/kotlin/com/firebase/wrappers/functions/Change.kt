package com.firebase.wrappers.functions

external interface Change<T> {
    val after: T
    val before: T
}