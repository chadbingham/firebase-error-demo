package com.firebase.wrappers.functions

external interface EventContext {
    val params: dynamic
}