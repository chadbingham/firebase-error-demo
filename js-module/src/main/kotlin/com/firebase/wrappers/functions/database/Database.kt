package com.firebase.wrappers.functions.database

external interface Database {
    fun ref(path: String): RefBuilder
}