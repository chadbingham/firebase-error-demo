package com.firebase.wrappers.functions

import com.firebase.wrappers.functions.database.Database

@JsModule("firebase-functions")
external object Functions {

    fun config(): dynamic

    val https: Https
    val database: Database
}

