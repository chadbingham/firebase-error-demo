package com.firebase.wrappers.functions.database

import com.firebase.wrappers.admin.CloudFunction
import com.firebase.wrappers.admin.Event
import com.firebase.wrappers.admin.database.DataSnapshot
import com.firebase.wrappers.functions.Change
import com.firebase.wrappers.functions.EventContext
import kotlin.js.Promise

external interface RefBuilder {
    fun onWrite(handler: (Event<DataSnapshot>, EventContext) -> Promise<Any>): CloudFunction<Change<DataSnapshot>>
    fun onUpdate(handler: (Change<DataSnapshot>, EventContext) -> Promise<Any>): CloudFunction<Change<DataSnapshot>>
    fun onCreate(handler: (Event<DataSnapshot>, EventContext) -> Promise<Any>): CloudFunction<DataSnapshot>
    fun onDelete(handler: (Change<DataSnapshot>, EventContext) -> Promise<Any>): CloudFunction<DataSnapshot>
}