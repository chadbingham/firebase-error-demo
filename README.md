**Setup:** 

* Add a json file with firebase test environment credentials at `js-module/`  
* Replace `JSON_FILE` and other credentials in `/js-module/src/test/kotlin/sample/TestFirebaseEnv.kt`
* Install node modules: 

    `cd  js-module/functions && npm install && cd -`
    
* Run test: 

    `./gradlew :js-module:build`

_______

**Output:** 

```
  1) sample
       SampleTest
         writeObject:
     Error: Argument "data" is not a valid Document. Input is not a plain JavaScript object.
      at Validator.(anonymous function).values [as isDocument] (functions/node_modules/@google-cloud/firestore/build/src/validate.js:99:27)
      at WriteBatch.set (functions/node_modules/@google-cloud/firestore/build/src/write-batch.js:232:25)
      at DocumentReference.set (functions/node_modules/@google-cloud/firestore/build/src/reference.js:349:27)
      at SampleTest.writeObject (functions/src/test/index_test.js:22:68)
      at Context.<anonymous> (functions/src/test/index_test.js:54:35)

```

[This was reported](https://github.com/firebase/firebase-functions/issues/344)